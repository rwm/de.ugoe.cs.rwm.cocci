/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.cocci;

import org.eclipse.emf.ecore.resource.Resource;

import de.ugoe.cs.rwm.cocci.identitycomp.SimpleComparator;
import de.ugoe.cs.rwm.cocci.similaritycomp.ComplexComparator;
import de.ugoe.cs.rwm.cocci.similaritycomp.MixedComparator;

//import de.ugoe.cs.oco.occi2deployment.connector.Connection;

public class ComparatorFactory {
	/**
	 * Returns a comparator instance. Comparing model1 (sourceModel) with
	 * model2(targetModel). A connection has to be passed due to the requirements of
	 * a idSwapList for the Simple/Mixed comparison approach.
	 *
	 * @param criteria
	 *            accepts "Simple","Emf","Complex","POG".
	 * @param model1
	 * @param model2
	 * @return Returns a comparator instance.
	 */
	/*
	 * public static Comparator getComparator(String criteria, Path model1, Path
	 * model2){ if(criteria.equals("Simple")){ //return new SimpleComparator(model1,
	 * model2, conn); } if(criteria.equals("Complex")){ return new
	 * ComplexComparator(model1, model2); } if(criteria.equals("Mixed")){ //return
	 * new MixedComparator(model1, model2); } return null; }
	 */

	public static Comparator getComparator(String criteria, Resource model1, Resource model2) {
		if (criteria.equals("Simple")) {
			return new SimpleComparator(model1, model2);
		} else if (criteria.equals("Complex")) {
			return new ComplexComparator(model1, model2);
		} else if (criteria.equals("Mixed")) {
			return new MixedComparator(model1, model2);
		} else {
			throw new IllegalArgumentException("Invalid Comparator Name");
		}
	}
}
