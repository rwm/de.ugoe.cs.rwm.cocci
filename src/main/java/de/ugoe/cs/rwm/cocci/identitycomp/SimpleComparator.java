/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.cocci.identitycomp;

import java.nio.file.Path;

import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

//import de.ugoe.cs.oco.occi2deployment.connector.Connection;
import de.ugoe.cs.rwm.cocci.AbsComparator;
import de.ugoe.cs.rwm.cocci.ModelUtility;
import de.ugoe.cs.rwm.cocci.match.Match;

/**
 * A simple comparator that compares two Models, by matching elements that
 * posses the same ids.
 *
 * @author rockodell
 *
 */
public class SimpleComparator extends AbsComparator {

	/**
	 * Creates a comparator instance based on the models passed as model1 and
	 * model2.
	 *
	 * @param model1
	 * @param model2
	 */
	public SimpleComparator(org.eclipse.emf.ecore.resource.Resource model1,
			org.eclipse.emf.ecore.resource.Resource model2) {
		compare(model1, model2);
	}

	protected void createResourceMatch(org.eclipse.emf.ecore.resource.Resource oldModelResource,
			org.eclipse.emf.ecore.resource.Resource newModelResource) {
		matchOldAndNewElements(ModelUtility.getOCCIConfigurationContents(oldModelResource),
				ModelUtility.getOCCIConfigurationContents(newModelResource));
		matchMissingElements(ModelUtility.getOCCIConfigurationContents(oldModelResource),
				ModelUtility.getOCCIConfigurationContents(newModelResource));
	}

	/**
	 * Fills the match of the comparator.
	 *
	 * @param oldModel
	 * @param newModel
	 */
	public void createResourceMatch(Path oldModelPath, EList<EObject> oldModel, Path newModelPath,
			EList<EObject> newModel) {
		matchOldAndNewElements(oldModel, newModel);
		matchMissingElements(oldModel, newModel);
	}

	/**
	 * Searches for missing elements in the newModel. And matches the oldModel
	 * elements missing to a null value.
	 *
	 * @param oldModel
	 * @param newModel
	 */
	private void matchMissingElements(EList<EObject> oldModel, EList<EObject> newModel) {
		boolean missingElement;
		for (Resource resourceFromOld : ModelUtility.getResources(oldModel)) {
			missingElement = true;
			for (Resource resourceFromNew : ModelUtility.getResources(newModel)) {
				if (resourceFromOld.getId().equals(resourceFromNew.getId())) {
					missingElement = false;
				}
			}
			if (missingElement) {
				Match match = new Match(resourceFromOld, null);
				matches.add(match);
			}
		}
	}

	/**
	 * Creates Matches for entities with the same id occuring in both models. If an
	 * id is not found, a match is created storing a null value and the entity from
	 * the new model.
	 *
	 * @param oldModel
	 * @param newModel
	 */
	private void matchOldAndNewElements(EList<EObject> oldModel, EList<EObject> newModel) {
		boolean newElement;
		for (Resource resourceFromNew : ModelUtility.getResources(newModel)) {
			newElement = true;
			for (Resource resourceFromOld : ModelUtility.getResources(oldModel)) {
				if (resourceFromOld.getId().equals(resourceFromNew.getId())) {
					newElement = false;
					Match match = new Match(resourceFromOld, resourceFromNew);
					matches.add(match);
				}
			}
			if (newElement) {
				Match match = new Match(null, resourceFromNew);
				matches.add(match);
			}
		}
	}
}
