/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.cocci;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import de.ugoe.cs.rwm.cocci.match.Match;

/**
 * Comparator interface defining methods to receive comparison results.
 *
 * @author rockodell
 *
 */
public interface Comparator {
	public EList<EObject> getNewElements();

	public EList<EObject> getOldElements();

	public EList<EObject> getMissingElements();

	public EList<EObject> getAdaptedElements();

	public EList<Match> getMatches();

}
