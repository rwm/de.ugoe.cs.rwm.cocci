/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.cocci.similaritycomp;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;

import de.ugoe.cs.rwm.cocci.AbsComparator;
import de.ugoe.cs.rwm.cocci.ModelUtility;
import de.ugoe.cs.rwm.cocci.match.Match;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;
import pcg.Vertex;

/**
 * Complex Comparator implements the similiarity flooding algorithm results as
 * intial weighting.
 *
 * @author rockodell
 *
 */
public class ComplexComparator extends AbsComplexComparator {
	/**
	 * Constructor executing the compare method to fill the ELists of the Object.
	 *
	 * @param oldModelPath
	 * @param newModelPath
	 */
	public ComplexComparator(Path oldModelPath, Path newModelPath) {
		compare(oldModelPath, newModelPath);
	}

	/**
	 * Empty Constructor
	 *
	 */
	public ComplexComparator() {
		// TODO Auto-generated constructor stub
	}

	public ComplexComparator(Resource oldModel, Resource newModel) {
		compare(oldModel, newModel);
	}

	protected void createResourceMatch(Path oldModelPath, EList<EObject> oldModel, Path newModelPath,
			EList<EObject> newModel) {
		Transformator occiToPcg = TransformatorFactory.getTransformator("OCCI2PCG");
		Path pcgPath = Paths.get(AbsComparator.getPathToResource("pcg/PCG.pcg"));
		try {
			occiToPcg.transform(oldModelPath, newModelPath, pcgPath);
		} catch (EolRuntimeException e) {
			LOG.error("OCCI2PCG Transformation could not be performed." + e);
		}

		Path ipgPath = Paths.get(AbsComparator.getPathToResource("ipg/IPG.pcg"));
		Transformator pcgToIpg = TransformatorFactory.getTransformator("PCG2IPG");
		try {
			pcgToIpg.transform(pcgPath, ipgPath);
		} catch (EolRuntimeException e) {
			LOG.error("PCG2IPG Transformation could not be performed." + e);
		}

		this.matches = generateMatches(ipgPath, oldModel, newModel);
	}

	protected void createResourceMatch(Resource oldModelResource, Resource newModelResource) {
		Transformator occiToPcg = TransformatorFactory.getTransformator("OCCI2PCG");
		Path pcgPath = Paths.get(AbsComparator.getPathToResource("pcg/PCG.pcg"));
		try {
			occiToPcg.transform(oldModelResource, newModelResource, pcgPath);
		} catch (EolRuntimeException e) {
			LOG.error("OCCI2PCG Transformation could not be performed." + e);
		}

		Path ipgPath = Paths.get(AbsComparator.getPathToResource("ipg/IPG.pcg"));
		Transformator pcgToIpg = TransformatorFactory.getTransformator("PCG2IPG");
		try {
			pcgToIpg.transform(pcgPath, ipgPath);
		} catch (EolRuntimeException e) {
			LOG.error("PCG2IPG Transformation could not be performed." + e);
		}

		this.matches = generateMatches(ipgPath, ModelUtility.getOCCIConfigurationContents(oldModelResource),
				ModelUtility.getOCCIConfigurationContents(newModelResource));
	}

	/**
	 * Returns generated matches from the ipgPath. Therefore, this method triggers
	 * the calculation of the fixpoint values from the IPG.
	 *
	 * @param ipgPath
	 * @param oldModel
	 * @param newModel
	 * @return
	 */
	EList<Match> generateMatches(Path ipgPath, EList<EObject> oldModel, EList<EObject> newModel) {
		Map<String, List<Vertex>> map = calculateFixpointValueMap(ipgPath);
		return createMatch(map, oldModel, newModel);
	}

	/**
	 * Returns the highest fixpoint value of the whole map.
	 *
	 * @param map
	 * @return Highest fixpoint value from the map.
	 */
	@Override
	protected Vertex getSuitableFixpointValue(Map<String, List<Vertex>> map, EList<EObject> oldModel,
			EList<EObject> newModel) {
		Vertex maxVertex = null;
		double max = 0.0;
		for (List<Vertex> vertices : map.values()) {
			sortVertices(vertices);
			logList(vertices);
			if (vertices.isEmpty() == false && vertices.get(0).getFixpointValue() > max) {
				maxVertex = vertices.get(0);
				max = vertices.get(0).getFixpointValue();
			}
		}
		return maxVertex;
	}
}
