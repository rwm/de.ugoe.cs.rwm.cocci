/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.cocci;

import java.io.File;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.cmf.occi.core.Configuration;
import org.eclipse.cmf.occi.core.Entity;
import org.eclipse.cmf.occi.core.Extension;
import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.util.OCCIResourceFactoryImpl;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class ModelUtility {
	/**
	 * Loads EObjects contained within the Configuration of the OCCI Model. If no
	 * Configuration is available every EObject of the model is loaded.
	 *
	 * @param path
	 *            to the OCCI Model
	 * @return OCCI Model as List of EObjects
	 */
	public static EList<EObject> loadOCCI(Path path) {
		OCCIPackage.eINSTANCE.eClass();
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;

		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("occie", new OCCIResourceFactoryImpl());

		ResourceSet resSet = new ResourceSetImpl();

		String file = new File(path.toString()).getAbsolutePath();
		URI fileURI = URI.createFileURI(file);
		Resource resource = resSet.getResource(fileURI, true);

		resSet.getResources().add(resource);

		EcorePlugin.ExtensionProcessor.process(null);
		for (EObject obj : resource.getContents()) {
			if (obj instanceof Configuration) {
				return ((Configuration) obj).eContents();
			}
		}

		return resource.getContents();
	}

	public static EList<EObject> getOCCIConfigurationContents(Resource resource) {
		for (EObject obj : resource.getContents()) {
			if (obj instanceof Configuration) {
				return ((Configuration) obj).eContents();
			}
		}
		return null;
	}

	/**
	 * Checks whether the element instantiates Entity.
	 *
	 * @param element
	 * @return True if the EObject inherits an OCCI Entity.
	 */
	public static boolean checkIfEntityElement(EObject element) {
		if (element instanceof org.eclipse.cmf.occi.core.Entity) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns all of the models top level and nested Resources as EObject List.
	 *
	 * @param model
	 *            OCCI Model
	 * @return EObject List containing all Entity elements contained in the model
	 */
	public static EList<org.eclipse.cmf.occi.core.Resource> getResources(EList<EObject> model) {
		EList<EObject> entities = new BasicEList<EObject>();
		EList<org.eclipse.cmf.occi.core.Resource> resources = new BasicEList<org.eclipse.cmf.occi.core.Resource>();
		entities.addAll(getTopLevelEntities(model));
		entities.addAll(getNestedEntities(model));
		for (EObject entity : entities) {
			if (entity instanceof org.eclipse.cmf.occi.core.Resource) {
				// Guess that is not correct
				resources.add((org.eclipse.cmf.occi.core.Resource) entity);
			}
		}
		return resources;
	}

	/**
	 * Returns Top level Entities in the Model.
	 *
	 * @param model
	 * @return
	 */
	private static EList<EObject> getTopLevelEntities(EList<EObject> model) {
		EList<EObject> entities = new BasicEList<EObject>();
		for (EObject element : model) {
			if (checkIfEntityElement(element)) {
				entities.add(element);
			}
		}
		return entities;
	}

	/**
	 * Returns nested Entities in the Model.
	 *
	 * @param model
	 * @return List of nested Entities of the OCCI Model.
	 */
	private static EList<EObject> getNestedEntities(EList<EObject> model) {
		EList<EObject> entities = new BasicEList<EObject>();
		for (EObject topLevelElement : getTopLevelEntities(model)) {
			for (Iterator<EObject> iterator = ((Entity) topLevelElement).eAllContents(); iterator.hasNext();) {
				EObject nested = iterator.next();
				if (checkIfEntityElement(nested)) {
					entities.add(nested);
				}
			}
		}
		return entities;
	}

	/**
	 * Returns all of the models top level and nested Entities as EObject List.
	 *
	 * @param model
	 * @return EObject List containing all Entity elements contained in the model
	 */
	public static EList<EObject> getEntities(EList<EObject> model) {
		EList<EObject> entities = new BasicEList<EObject>();
		entities.addAll(getTopLevelEntities(model));
		entities.addAll(getNestedEntities(model));
		return entities;
	}

	public static Resource loadOCCIResource(Path configuration, List<Path> extensions) {
		// InfrastructurePackage.eINSTANCE.eClass();
		OCCIPackage.eINSTANCE.eClass();
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;

		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("occie", new OCCIResourceFactoryImpl());
		m.put("occic", new OCCIResourceFactoryImpl());

		ResourceSet resSet = new ResourceSetImpl();

		if (extensions != null) {
			for (Path path : extensions) {
				String filePath = new File(path.toString()).getAbsolutePath();
				Resource extResource = resSet.getResource(URI.createFileURI(filePath), true);
				if (extResource.getContents().get(0) instanceof Extension) {
					Extension ext = (Extension) extResource.getContents().get(0);
					URI realURI = URI.createURI(ext.getScheme()).trimFragment();
					extResource.setURI(realURI);
				}
				resSet.getResources().add(extResource);
			}
		}

		String file = new File(configuration.toString()).getAbsolutePath();
		Resource resource = resSet.getResource(URI.createFileURI(file), true);

		/*
		 * String file = new File(configuration.toString()).getAbsolutePath(); URI
		 * fileURI = URI.createFileURI(file); Resource resource =
		 * resSet.getResource(fileURI, true);
		 */

		EcorePlugin.ExtensionProcessor.process(null);
		EcoreUtil.resolveAll(resSet);

		return resource;
	}

	public static EList<EObject> loadOCCI(Path configuration, List<Path> extensions) {
		// InfrastructurePackage.eINSTANCE.eClass();
		OCCIPackage.eINSTANCE.eClass();
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;

		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("occie", new OCCIResourceFactoryImpl());
		m.put("occic", new OCCIResourceFactoryImpl());

		ResourceSet resSet = new ResourceSetImpl();

		if (extensions != null) {
			for (Path path : extensions) {
				String filePath = new File(path.toString()).getAbsolutePath();
				Resource extResource = resSet.getResource(URI.createFileURI(filePath), true);
				if (extResource.getContents().get(0) instanceof Extension) {
					Extension ext = (Extension) extResource.getContents().get(0);
					URI nsURI = URI.createURI(ext.getScheme()).trimFragment(); // #
					extResource.setURI(nsURI);

				}
				resSet.getResources().add(extResource);
			}
		}

		String file = new File(configuration.toString()).getAbsolutePath();
		// URI fileURI = URI.createURI(path.toString());
		URI fileURI = URI.createFileURI(file);
		Resource resource = resSet.getResource(fileURI, true);

		EcorePlugin.ExtensionProcessor.process(null);
		EcoreUtil.resolveAll(resSet);
		// System.out.println(UnresolvedProxyCrossReferencer.find(resource));

		for (EObject obj : resource.getContents()) {
			if (obj instanceof Configuration) {
				return ((Configuration) obj).eContents();
			}
		}

		return resource.getContents();
	}
}
