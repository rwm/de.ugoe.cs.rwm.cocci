/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/

package de.ugoe.cs.rwm.cocci.match;

import org.eclipse.emf.ecore.EObject;

/**
 * An entity class storing information about two matching Objects.
 *
 * @author rockodell
 *
 */
public class Match {
	private EObject src;
	private EObject tar;

	/**
	 * Default Constructor Creates an Empty Match.
	 *
	 */
	public Match() {
	}

	/**
	 * Constructor typically used to instantiate a Match. Matches oldObj to newObj.
	 *
	 * @param src
	 * @param tar
	 */
	public Match(EObject src, EObject tar) {
		this.src = src;
		this.tar = tar;
	}

	/**
	 * Sets the src of the Match.
	 *
	 * @param src
	 */
	public void setOldObj(EObject src) {
		this.src = src;
	}

	/**
	 * Sets the tar of the Match.
	 *
	 * @param tar
	 */
	public void setNewObj(EObject tar) {
		this.tar = tar;
	}

	/**
	 * Returns the src of the Match.
	 *
	 * @return Src of the Match.
	 */
	public EObject getSrc() {
		return this.src;
	}

	/**
	 * Returns the tar of the Match.
	 *
	 * @return Target of the Match.
	 */
	public EObject getTar() {
		return this.tar;
	}
}
