package de.ugoe.cs.rwm.cocci;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.ugoe.cs.rwm.cocci.match.Match;

public class LogMatchTest {
	TestComparator comp = new TestComparator();
	List<Match> matches = new ArrayList<Match>();

	@Before
	public void OCCIRegistrySetup() {
		matches.clear();
	}

	@Test
	public void logEmptyMatch() {
		Match match = new Match();
		matches.add(match);
		comp.logMatch(matches);
		assertTrue(true);
	}

	@Test
	public void logEmptyList() {
		comp.logMatch(matches);
		assertTrue(true);
	}

	@Test
	public void logNullMatch() {
		Match match = new Match(null, null);
		matches.add(match);
		comp.logMatch(matches);
		assertTrue(true);
	}
}
