/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.cocci;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.cmf.occi.core.Link;
import org.eclipse.cmf.occi.core.Mixin;
import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.Resource;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.cmf.occi.infrastructure.InfrastructurePackage;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.modmacao.placement.PlacementPackage;

import modmacao.ModmacaoPackage;
import openstackruntime.OpenstackruntimePackage;
import ossweruntime.OssweruntimePackage;

public class ProxyResolutionTest {

	@BeforeClass
	public static void OCCIRegistrySetup() {
		TestUtility.extensionRegistrySetup();
	}

	@Test
	public void resolveProxies() {
		Path newOCCI = Paths.get(AbsComparator.getPathToResource("occi/WaaS.occic"));
		EList<EObject> newModel = ModelUtility.loadOCCI(newOCCI, null);
		for (Resource res : ModelUtility.getResources(newModel)) {
			System.out.println(res.getKind());
			/*
			 * for(Attribute attr: res.getKind().getAttributes()) {
			 * System.out.println(attr); }
			 */
			for (Mixin mix : res.getMixins()) {
				System.out.println(mix);
				System.out.println(mix.getScheme());
			}
			
			for(Link link: res.getLinks()) {
				System.out.println(link.getKind());
			}
		}
	}
}
