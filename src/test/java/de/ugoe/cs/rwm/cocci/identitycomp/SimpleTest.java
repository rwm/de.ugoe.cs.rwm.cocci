/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.cocci.identitycomp;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.cmf.occi.infrastructure.InfrastructurePackage;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.junit.BeforeClass;
import org.junit.Test;
import org.modmacao.placement.PlacementPackage;

import de.ugoe.cs.rwm.cocci.AbsComparator;
import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.cocci.ComparatorFactory;
import de.ugoe.cs.rwm.cocci.ModelUtility;
import de.ugoe.cs.rwm.cocci.TestUtility;
import modmacao.ModmacaoPackage;
import openstackruntime.OpenstackruntimePackage;
import ossweruntime.OssweruntimePackage;
import workflow.WorkflowPackage;

public class SimpleTest {
	private final static String VERSION = "Simple";

	@BeforeClass
	public static void OCCIRegistrySetup() {
		CachedResourceSet.getCache().clear();
		TestUtility.extensionRegistrySetup();
	}

	@Test
	public void simpleCompareInfra() {
		System.out.println("MLS -> MLS:");
		Path occiPath = Paths.get(AbsComparator.getPathToResource("occi/Infra.occic"));
		Path occiPath2 = Paths.get(AbsComparator.getPathToResource("occi/Infra2.occic"));

		org.eclipse.emf.ecore.resource.Resource oldModelResource = TestUtility.loadOCCIResource(occiPath);
		org.eclipse.emf.ecore.resource.Resource newModelResource = TestUtility.loadOCCIResource(occiPath2);

		Comparator comparator = ComparatorFactory.getComparator(VERSION, oldModelResource, newModelResource);
		CachedResourceSet.getCache().clear();
		System.out.println(comparator.getMatches());

		assertTrue(true);
	}

	@Test
	public void simpleComparePaaS() {
		System.out.println("MLS -> MLS:");
		Path occiPath = Paths.get(AbsComparator.getPathToResource("occi/PaaSNew.occic"));
		Path occiPath2 = Paths.get(AbsComparator.getPathToResource("occi/PaaSOld.occic"));

		org.eclipse.emf.ecore.resource.Resource oldModelResource = TestUtility.loadOCCIResource(occiPath);
		org.eclipse.emf.ecore.resource.Resource newModelResource = TestUtility.loadOCCIResource(occiPath2);

		Comparator comparator = ComparatorFactory.getComparator(VERSION, oldModelResource, newModelResource);
		CachedResourceSet.getCache().clear();
		System.out.println(comparator.getMatches());

		assertTrue(true);
	}

	@Test
	public void loggingException() {

		System.out.println("MLS -> MLS:");
		Path occiPath = Paths.get(AbsComparator.getPathToResource("occi/emptyLinkTarget.occic"));
		Path occiPath2 = Paths.get(AbsComparator.getPathToResource("occi/emptyLinkTarget.occic"));

		org.eclipse.emf.ecore.resource.Resource oldModelResource = TestUtility.loadOCCIResource(occiPath);
		org.eclipse.emf.ecore.resource.Resource newModelResource = TestUtility.loadOCCIResource(occiPath2);

		Comparator comparator = ComparatorFactory.getComparator(VERSION, oldModelResource, newModelResource);
		CachedResourceSet.getCache().clear();
		System.out.println(comparator.getMatches());

		assertTrue(true);
	}
}