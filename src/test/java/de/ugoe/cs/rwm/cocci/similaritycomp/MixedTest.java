/*******************************************************************************
 * Copyright (c) 2019 University of Goettingen.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     - Johannes Erbel <johannes.erbel@cs.uni-goettingen.de>
 *******************************************************************************/
package de.ugoe.cs.rwm.cocci.similaritycomp;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.cmf.occi.core.OCCIPackage;
import org.eclipse.cmf.occi.core.util.OcciRegistry;
import org.eclipse.cmf.occi.infrastructure.InfrastructurePackage;
import org.eclipse.epsilon.emc.emf.CachedResourceSet;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.modmacao.placement.PlacementPackage;

import de.ugoe.cs.rwm.cocci.AbsComparator;
import de.ugoe.cs.rwm.cocci.Comparator;
import de.ugoe.cs.rwm.cocci.ComparatorFactory;
import de.ugoe.cs.rwm.cocci.ModelUtility;
import de.ugoe.cs.rwm.tocci.Transformator;
import de.ugoe.cs.rwm.tocci.TransformatorFactory;
import modmacao.ModmacaoPackage;
import openstackruntime.OpenstackruntimePackage;
import ossweruntime.OssweruntimePackage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MixedTest {

	private static final String VERSION = "Mixed";

	@BeforeClass
	public static void OCCIRegistrySetup() {
		Logger.getLogger(Comparator.class.getName()).setLevel(Level.DEBUG);
		Logger.getRootLogger().setLevel(Level.FATAL);

		InfrastructurePackage.eINSTANCE.eClass();
		OCCIPackage.eINSTANCE.eClass();
		ModmacaoPackage.eINSTANCE.eClass();
		OpenstackruntimePackage.eINSTANCE.eClass();
		PlacementPackage.eINSTANCE.eClass();

		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/modmacao#",
				ModmacaoPackage.class.getClassLoader().getResource("model/modmacao.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/openstack/runtime#",
				OpenstackruntimePackage.class.getClassLoader().getResource("model/openstackruntime.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.modmacao.org/openstack/swe#",
				OssweruntimePackage.class.getClassLoader().getResource("model/openstackruntime.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.occiware.org/placement#",
				PlacementPackage.class.getClassLoader().getResource("model/placement.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/infrastructure#",
				InfrastructurePackage.class.getClassLoader().getResource("model/Infrastructure.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ogf.org/occi/core#",
				OCCIPackage.class.getClassLoader().getResource("model/Core.occie").toString());
		OcciRegistry.getInstance().registerExtension("http://schemas.ugoe.cs.rwm/workflow#",
				AbsComparator.getPathToResource("occi/workflow.occie"));
	}

	@Test
	public void mixedCompareInfra() {
		List<Path> extensions = new ArrayList<Path>();

		System.out.println("MLS -> MLS:");
		Path occiPath = Paths.get(AbsComparator.getPathToResource("occi/Infra.occic"));
		Path occiPath2 = Paths.get(AbsComparator.getPathToResource("occi/Infra2.occic"));

		org.eclipse.emf.ecore.resource.Resource oldModelResource = ModelUtility.loadOCCIResource(occiPath, null);
		org.eclipse.emf.ecore.resource.Resource newModelResource = ModelUtility.loadOCCIResource(occiPath2, extensions);

		Comparator comparator = ComparatorFactory.getComparator(VERSION, oldModelResource, newModelResource);
		CachedResourceSet.getCache().clear();
		System.out.println(comparator.getMatches());

		assertTrue(true);
	}

	@Test
	public void mixedComparePaaS() {
		List<Path> extensions = new ArrayList<Path>();

		System.out.println("MLS -> MLS:");
		Path occiPath = Paths.get(AbsComparator.getPathToResource("occi/PaaSNew.occic"));
		Path occiPath2 = Paths.get(AbsComparator.getPathToResource("occi/PaaSOld.occic"));

		org.eclipse.emf.ecore.resource.Resource oldModelResource = ModelUtility.loadOCCIResource(occiPath, null);
		org.eclipse.emf.ecore.resource.Resource newModelResource = ModelUtility.loadOCCIResource(occiPath2, extensions);

		Comparator comparator = ComparatorFactory.getComparator(VERSION, oldModelResource, newModelResource);
		CachedResourceSet.getCache().clear();
		System.out.println(comparator.getMatches());

		assertTrue(true);
	}

	@Test
	public void mixedCompareWaaS() {
		List<Path> extensions = new ArrayList<Path>();

		System.out.println("WaaS -> WaaS:");
		Path occiPath = Paths.get(AbsComparator.getPathToResource("occi/WaaS.occic"));
		Path occiPath2 = Paths.get(AbsComparator.getPathToResource("occi/WaaS.occic"));

		org.eclipse.emf.ecore.resource.Resource oldModelResource = ModelUtility.loadOCCIResource(occiPath, null);
		org.eclipse.emf.ecore.resource.Resource newModelResource = ModelUtility.loadOCCIResource(occiPath2, extensions);

		System.out.println(oldModelResource.getContents().get(0).eContents());

		Comparator comparator = ComparatorFactory.getComparator(VERSION, oldModelResource, newModelResource);
		CachedResourceSet.getCache().clear();
		System.out.println(comparator.getMatches());

		assertTrue(true);
	}

	@Test
	public void waaSToWaaSWOExecLink() throws EolRuntimeException {

		List<Path> extensions = new ArrayList<Path>();

		Transformator trans = TransformatorFactory.getTransformator("OCCI2OCCI");

		System.out.println("WaaS -> WaaS:");
		Path occiPath = Paths.get(AbsComparator.getPathToResource("occi/WaaSWOExecLink.occic"));
		trans.transform(occiPath, occiPath);
		Path occiPath2 = Paths.get(AbsComparator.getPathToResource("occi/WaaS.occic"));

		org.eclipse.emf.ecore.resource.Resource oldModelResource = ModelUtility.loadOCCIResource(occiPath, null);
		org.eclipse.emf.ecore.resource.Resource newModelResource = ModelUtility.loadOCCIResource(occiPath2, null);

		Comparator comparator = ComparatorFactory.getComparator(VERSION, oldModelResource, newModelResource);
		CachedResourceSet.getCache().clear();
		System.out.println(comparator.getMatches());

		assertTrue(true);
	}
	// TODO: Test case with semi-different ids
	// TODO: Test case with complete different ids
}