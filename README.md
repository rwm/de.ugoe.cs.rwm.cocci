# COCCI

[![pipeline status](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cocci/badges/master/pipeline.svg)](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cocci/commits/master)
[![coverage report](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cocci/badges/master/coverage.svg)](https://gitlab.gwdg.de/rwm/de.ugoe.cs.rwm.cocci/commits/master)

COCCI is a library consisting of Model Comparisons for OCCI models.
Hereby, the following procedeure is performed for all comparison approaches.
First, all OCCI Resources from one model are matched to another one based on the comparison approaches enumerated beneath.
Thereafter, the individual OCCI Links contained within the matched Resources are compared.

Currently the following comparisons are part of COCCI:

## Static-identity based
These comparison approaches are based on static-identity based analysis.

### Simple Comparator
The Simple Comparator compares each single element based on their ids.

## Similarity based
Similarity based approaches may derive comparisons and matchings via the similarity of models.
For example, attributes and structure can be compared.

### Complex comparator
The Complex Comparator implements the Similarity Flooding Algorithm comparing two models based on their structure.
Hereby, a graph representation is generated from the OCCI models to be compared via an Epsilon model transformation.

### Mixed Comparator
The Mixed Comparator combines the static-identity based Simple Comparator with the Complex Comparator implementing the Similarity-Flooding Algorithm.
Hereby, Resources are first directly matched via their id, before a structured based comparison of non matched Resources is performed.
